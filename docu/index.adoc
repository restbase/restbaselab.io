= restbase
:toc: left

Restbase is an HTTP-REST database.

include::getting-started.adoc[]

include::source-code.adoc[]

include::architecture.adoc[]

== Architecture

https://https://c4model.com/[C4 Model] software architecture definition

=== Level 1: System Context diagram

[plantuml,architecture-context,png]
----
include::architecture-context.adoc[]
----

=== Level 2: Container diagram

[plantuml,architecture-container,png]
----
include::architecture-container.adoc[]
----

=== Level 3: Component diagrams

Restbase client connects directly to the Restbase gateway, which redirects the requests to the Restbase Server and manages load-balancing.

[plantuml,architecture-component-gateway,png]
----
include::architecture-component-gateway.adoc[]
----

Restbase Server is a Spring Boot based Java application which uses JSON over HTTP/S to exchange data with the client application.

[plantuml,architecture-component-server,png]
----
include::architecture-component-server.adoc[]
----

Restbase Client is an npm/Node.js based client library which can be used as a dependency inside an Angular, Javascript based web application.

[plantuml,architecture-component-client,png]
----
include::architecture-component-client.adoc[]
----

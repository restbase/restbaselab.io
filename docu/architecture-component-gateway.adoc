@startuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Component.puml
' uncomment the following line and comment the first to use locally
' !include C4_Context.puml

LAYOUT_WITH_LEGEND()

title Restbase Gateway Component

Container(web_application, "Web Application", "Javascript, Angular", "A web application using restbase client to store data")
Container(restbase, "Restbase server", "Docker, Java, Spring Boot", "Restbase server")

Container_Boundary(gateway, "Restbase gateway") {

    Component(RestbaseGateway, "Gateway Application", "Spring Boot Application", "configures and starts Spring Boot application")

    Component(RoutingConfiguration, "Routing Configuration", "Spring Configuration", "Defines routing configuration")
    Component(RestbaseConfiguration, "Restbase Configuration", "Spring Configuration Properties", "Defines restbase configuration")
    
    Rel_L(RestbaseGateway, RestbaseConfiguration, "uses")
    Rel(RestbaseGateway, RoutingConfiguration, "uses")

}
Rel(web_application, RestbaseGateway, "JSON, HTTP/S")
Rel_R(RestbaseGateway, restbase, "JSON, HTTP/S")

@enduml
